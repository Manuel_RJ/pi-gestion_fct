/*----------------------------------
            DROPS Tables
----------------------------------*/

/*
DROP TABLE IF EXISTS ALUMNOS;
DROP TABLE IF EXISTS SEDES;
DROP TABLE IF EXISTS CANDIDATURAS;
DROP TABLE IF EXISTS EMPRESAS;
DROP TABLE IF EXISTS USERS;
*/

/*----------------------------------
        Create Table USERS
----------------------------------*/

Create Table USERS(
    ID INT(4) NOT NULL AUTO_INCREMENT,
    PASSWD VARCHAR(100) NOT NULL,
    NOMBRE VARCHAR(15) NOT NULL,
    APELLIDOS VARCHAR(30) NOT NULL,
    EDAD INT(3) NOT NULL,
    EMAIL VARCHAR(50) NOT NULL,
    DNI VARCHAR(9) NOT NULL,
    TELEFONO VARCHAR(9),
    ROL VARCHAR(13) NOT NULL,
    CONSTRAINT USER_PK PRIMARY KEY (ID)
);

/*----------------------------------
        Create Table EMPRESAS
----------------------------------*/

CREATE TABLE EMPRESAS (
    ID INT(4) NOT NULL AUTO_INCREMENT,
    NOMBRE VARCHAR(15) NOT NULL , 
    CIF VARCHAR(9) NOT NULL , 
    NUM_EMPLEADO INT(6) NOT NULL,
    CONSTRAINT EMPRESA_PK PRIMARY KEY (ID)
);

/*----------------------------------
        Create Table ALUMNOS
----------------------------------*/

Create Table ALUMNOS(
    ID INT(4) NOT NULL,
    CV VARCHAR(25),
    PROFESOR_SEGUIMIENTO_ID INT(4) NOT NULL,
    CONSTRAINT ALUMNO_PK PRIMARY KEY (ID)
);

/*----------------------------------
        Create Table SEDES
----------------------------------*/

Create Table SEDES(
    ID INT(4) NOT NULL AUTO_INCREMENT,
    NOMBRE VARCHAR(15) NOT NULL,
    DIRECCION VARCHAR(150) NOT NULL,
    TELEFONO VARCHAR(9) NOT NULL,
    EMPRESA_ID INT(4) NOT NULL,
    CONSTRAINT SEDE_PK PRIMARY KEY (ID)
);

/*----------------------------------
     Create Table CANDIDATURAS
----------------------------------*/

Create Table CANDIDATURAS(
    ID INT(4) NOT NULL AUTO_INCREMENT,
    ESTADO VARCHAR(15) NOT NULL,
    ALUMNO_ID INT(4) NOT NULL,
    EMPRESA_ID INT(4) NOT NULL,
    CONSTRAINT CANDIDATURA_PK PRIMARY KEY (ID)
);

/*----------------------------------
            COMMIT
----------------------------------*/

COMMIT;


