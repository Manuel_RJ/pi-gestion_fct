/*----------------------------------
        Alter Table USERS
----------------------------------*/

ALTER TABLE USERS ADD CONSTRAINT UC_USER UNIQUE (EMAIL,DNI,TELEFONO);
ALTER TABLE USERS ADD CHECK (ROL IN('docente','alumno'));

/*----------------------------------
        Alter Table EMPRESAS
----------------------------------*/

ALTER TABLE EMPRESAS ADD UNIQUE (CIF);

/*----------------------------------
        Alter Table ALUMNOS
----------------------------------*/

ALTER TABLE ALUMNOS
ADD CONSTRAINT FK1_ALUMNO
FOREIGN KEY (ID) REFERENCES USERS(ID);

ALTER TABLE ALUMNOS
ADD CONSTRAINT FK2_ALUMNO
FOREIGN KEY (PROFESOR_SEGUIMIENTO_ID) REFERENCES USERS(ID);


/*----------------------------------
        Alter Table SEDES
----------------------------------*/

ALTER TABLE SEDES
ADD CONSTRAINT FK_SEDE
FOREIGN KEY (EMPRESA_ID) REFERENCES EMPRESAS(ID);

ALTER TABLE SEDES ADD UNIQUE (TELEFONO);

/*----------------------------------
     Alter Table CANDIDATURAS
----------------------------------*/

ALTER TABLE CANDIDATURAS
ADD CONSTRAINT FK1_CANDIDATURA
FOREIGN KEY (ALUMNO_ID) REFERENCES ALUMNOS(ID);

ALTER TABLE CANDIDATURAS
ADD CONSTRAINT FK2_CANDIDATURA
FOREIGN KEY (EMPRESA_ID) REFERENCES EMPRESAS(ID);

ALTER TABLE CANDIDATURAS ADD CHECK (ESTADO IN('pendiente','rechazada','aceptada'));

/*----------------------------------
            COMMIT
----------------------------------*/
COMMIT;
