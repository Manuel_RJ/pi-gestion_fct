/*----------------------------------
        Insert Table USERS
----------------------------------*/

INSERT into USERS(ID,NICKNAME,PASSWD,NOMBRE,APELLIDOS,EDAD,EMAIL,DNI,TELEFONO,ROL) 
values(1,"admin123","manuel","Ruiz Jimenez",21,"manuelruizjimenezmrj01@gmail.com","77834164S",675495724,"docente");

INSERT into USERS(ID,NICKNAME,PASSWD,NOMBRE,APELLIDOS,EDAD,EMAIL,DNI,TELEFONO,ROL) 
values(2,"est123","prueba","prueba_apellido",21,"prueba@gmail.com","74832164L",675751234,"alumno");

/*----------------------------------
        Insert Table EMPRESAS
----------------------------------*/

INSERT into EMPRESAS(ID,NOMBRE,CIF,NUM_EMPLEADO) values(1,"empresa","B76345879",10000);

/*----------------------------------
        Insert Table ALUMNOS
----------------------------------*/

INSERT into ALUMNOS(ID,CV,PROFESOR_SEGUIMIENTO_ID) values(2,null,1);

/*----------------------------------
        Insert Table SEDES
----------------------------------*/

INSERT into SEDES(ID,NOMBRE,DIRECCION,TELEFONO,EMPRESA_ID) values(1,"Desarrollo","C/ Prueba Nº3",546827965,1);

/*----------------------------------
     Insert Table CANDIDATURAS
----------------------------------*/

INSERT into CANDIDATURAS(ID,ESTADO,ALUMNO_ID,EMPRESA_ID) values(1,"pendiente",2,1);

/*----------------------------------
            COMMIT
----------------------------------*/
COMMIT;
